# General tools #

Requirements
------------
 * PHP 5.5.6+

Installation
------------
Add the following to your composer.json:

```json
{
    "require": {
        "eightbitz/tools": "dev-master"
    },
    "repositories": [
        {
            "type": "vcs",
            "url":  "https://bitbucket.org/8bitz/tools.git"
        }
    ]
}
```

Run `composer install` or `composer update` when this library was updated.

Basic usage
-----------
```php
<?php

use Eightbitz\Tools\ImageResize;

$image = new ImageResize('/path/to/image/file.jpg');
$image->crop(100, 100);
$image->save('/path/to/image/file_new.jpg');

?>
```