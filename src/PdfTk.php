<?php

namespace Eightbitz\Tools;

class PdfTk
{
    public $bin = '/usr/bin/pdftk';

    protected $enableEscaping = true;
    protected $options = [];
    protected $input_file;
    protected $stamp_file;
    protected $background_file;
    protected $multi_background = false;
    protected $form_data_file;
    protected $form_file;
    protected $error;

    public function __construct($options = [])
    {
        if (!empty($options)) {
            $this->setOptions($options);
        }
    }

    public function setOptions($options)
    {
        foreach ($options as $key => $val) {
            if ($key === 'bin') {
                $this->bin = $val;
            } elseif ($key==='enableEscaping') {
                $this->enableEscaping = (bool)$val;
            }
        }
    }

    public function getError()
    {
        return $this->error;
    }

    public function setInput($file)
    {
        if (is_readable($file)) {
            $this->input_file = $file;
            return true;
        }

        $this->error = 'Cant read input file';
        return false;
    }

    public function setStamp($file)
    {
        if (is_readable($file)) {
            $this->stamp_file = $file;
            return true;
        }

        $this->error = 'Cant read stamp file';
        return false;
    }

    public function stampDocument($fileName)
    {
        $command = $this->enableEscaping ? escapeshellarg($this->bin) : $this->bin;
        $command .= ' ' . $this->input_file . ' stamp ' . $this->stamp_file . ' output ' . $fileName;

        $command = escapeshellcmd($command);

        $result = system($command);

        return ($this->error === null);
    }

    public function setBackground($file, $multi=false)
    {
        if (is_readable($file)) {
            $this->background_file = $file;
            $this->multi_background = $multi;
            return true;
        }

        $this->error = 'Cant read background file';
        return false;
    }

    public function addBackground($fileName)
    {
        $command = $this->enableEscaping ? escapeshellarg($this->bin) : $this->bin;
        $command .= ' ' . $this->input_file . ($this->multi_background ? ' multibackground ' : ' background ') . $this->background_file . ' output ' . $fileName;

        $command = escapeshellcmd($command);

        $result = system($command);

        return ($this->error === null);
    }

    public function getPage($fileName, $pagenr=1)
    {
        $range = 'A' . $pagenr;

        $command = $this->enableEscaping ? escapeshellarg($this->bin) : $this->bin;
        $command .= ' A=' . $this->input_file . ' cat ' . $range . ' output ' . $fileName;

        $command = escapeshellcmd($command);

        $result = system($command);

        return ($this->error === null);
    }

    public function setFormData($file)
    {
        if (is_readable($file)) {
            $this->form_data_file = $file;
            return true;
        }

        $this->error = 'Cant read form data file';
        return false;
    }

    public function fillForm($fileName)
    {
        $command = $this->enableEscaping ? escapeshellarg($this->bin) : $this->bin;
        $command .= ' ' . $this->input_file . ' fill_form ' . $this->form_data_file . ' output ' . $fileName;

        $command = escapeshellcmd($command);

        $result = system($command);

        return ($this->error === null);
    }

    public function combineFiles($input1, $input2, $output, $args = 'A B')
    {
        $command = $this->enableEscaping ? escapeshellarg($this->bin) : $this->bin;
        $command .= ' A=' . $input1 . ' B=' . $input2 . ' cat ' . $args . ' output ' . $output;

        $command = escapeshellcmd($command);

        $result = system($command);

        return ($this->error === null);
    }

    public function combineFilesArray($input, $output)
    {
        $command = $this->enableEscaping ? escapeshellarg($this->bin) : $this->bin;

        $command .= ' ' . join(' ', $input); // file1 file2 file3

        $command .= ' cat output ' . $output;

        $command = escapeshellcmd($command);

        $result = system($command);

        return ($this->error === null);
    }

    public function uncompressFile($fileName)
    {
        $command = $this->enableEscaping ? escapeshellarg($this->bin) : $this->bin;
        $command .= ' ' . $this->input_file . ' output ' . $fileName . ' uncompress';

        $command = escapeshellcmd($command);

        $result = system($command);

        return ($this->error === null);
    }
}
