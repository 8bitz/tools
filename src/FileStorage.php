<?php
/**
 * PHP class to store huge amount of files with subdirs etc.
 *
 *
 * Usage:
 *   $fs->add('/path/to/file.txt');
 *   $fs->delete('/path/to/file.txt');
 */
namespace Eightbitz\Tools;

class FileStorage {

    private $storageRoot = null;
    private $addHash = false;
    private $maxFiles = 10000;

    public function __construct($options = array()) {
        if(empty($options)) {
            throw new Exception('Pass options!');
        }
        $this->storageRoot = $options['root'];
        if(isset($options['add_hash'])) {
            $this->addHash = $options['add_hash'];
        }
    }

    private function getSubdir() {

        if(!is_dir($this->storageRoot)) {
            mkdir($this->storageRoot, 0777, true);
        }
        $cdir = scandir($this->storageRoot);
        $subDirs = array();
        foreach ($cdir as $key => $value) {
            if (!in_array($value, array(".", "..")) && is_dir($this->storageRoot.'/'.$value)) {
                $subDirs[] = $value;
            }
        }
        $subDirs = array_reverse($subDirs);
        if(empty($subDirs)) {
            mkdir($this->storageRoot.'1', 0777, true);
            $subDirs = array(1);
        }
        $lastDir = $this->storageRoot.'/'.current($subDirs);
        if(!$this->isDirFull($lastDir)) {
            return $this->storageRoot.(int) current($subDirs);
        } else {
            mkdir($this->storageRoot.((int) current($subDirs) + 1), 0777, true);
            return $this->storageRoot.((int) current($subDirs) + 1);
        }
    }
    private function isDirFull($dir) {
        if(!is_dir($dir)) {
            return false;
        }
        $fi = new FilesystemIterator($dir, FilesystemIterator::SKIP_DOTS);
        return iterator_count($fi) >= $this->maxFiles;
    }

    private function getHash() {
        $num = mt_rand ( 0, 0xffffff );
        $output = sprintf ( "%06x" , $num );
        return $output;
    }

    private function createSlug($string) {
        $table = array(
            'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
            'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
            'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
            'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
            'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
            'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
            'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', '/' => '-', ' ' => '-', '(' => '', ')' => '', '.' => ''
        );
        $stripped = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $string);
        return strtolower(strtr($string, $table));
    }

    public function add($file) {
        if(!file_exists($file)) {
            throw new Exception('File '. $file.' not found!');
        }
        $subdir = $this->getSubdir();

        $fileparts = pathinfo($file);

        $targetname = '';
        if($this->addHash === true) {
            $targetname .= $this->getHash().'-';
        }

        $targetname .= $this->createSlug($fileparts['filename']).'.'.$fileparts['extension'];
        if(!rename($file, $subdir.'/'.$targetname)) {
            throw new Exception('Could not move file to target storage');
        }
        $x = explode('/', $subdir);

        return new FileStorageFile($subdir.'/'.$targetname, $targetname, end($x));
    }

    public function delete($file) {
        if(file_exists($file)) {
            @unlink($file);
            return true;
        }
        return false;
    }
}

class FileStorageFile {
    public $fullPath;
    public $filename;
    public $subDir;

    public function __construct($fullPath, $filename, $subDir) {
        $this->fullPath = $fullPath;
        $this->filename = $filename;
        $this->subDir = $subDir;
    }
}